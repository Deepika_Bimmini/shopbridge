﻿using ShopBridgeProject.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShopBridgeProject.Controllers
{
    public class ShopsController : Controller
    {
        // GET: Shops
        ShopBridgeEntities db = new ShopBridgeEntities();
        public ActionResult Index()
        {
            return View(db.Shop_db.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file, Shop_db shop)
        {
            string fileName = Path.GetFileName(file.FileName);
            string _fileName = DateTime.Now.ToString("yymmssffff") + fileName;
            string extension = Path.GetExtension(file.FileName);
            string path = Path.Combine(Server.MapPath("~/images/"), _fileName);
            shop.ImagePath = "~/images/" + _fileName;

            if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
            {
                if (file.ContentLength <= 1000000)
                {
                    db.Shop_db.Add(shop);
                    if (db.SaveChanges() > 0)
                    {
                        file.SaveAs(path);
                        ModelState.Clear();
                    }
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop_db shop = db.Shop_db.Find(id);
            Session["imagePath"] = shop.ImagePath;

            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase file, Shop_db shop)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    string fileName = Path.GetFileName(file.FileName);
                    string _fileName = DateTime.Now.ToString("yymmssffff") + fileName;
                    string extension = Path.GetExtension(file.FileName);
                    string path = Path.Combine(Server.MapPath("~/images/"), _fileName);
                    shop.ImagePath = "~/images/" + _fileName;

                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
                    {
                        if (file.ContentLength <= 1000000)
                        {
                            db.Entry(shop).State = EntityState.Modified;
                            string oldImagePath = Request.MapPath(Session["imagePath"].ToString());
                            if (db.SaveChanges() > 0)
                            {
                                file.SaveAs(path);
                                if (System.IO.File.Exists(oldImagePath))
                                {
                                    System.IO.File.Delete(oldImagePath);
                                }
                            }
                        }
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    shop.ImagePath = Session["imagePath"].ToString();
                    db.Entry(shop).State = EntityState.Modified;
                    if (db.SaveChanges() > 0)
                    {
                        return RedirectToAction("Index");
                    }
                }
            }
            return View();
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop_db shop = db.Shop_db.Find(id);

            if (shop == null)
            {
                return HttpNotFound();
            }
            return View(shop);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Shop_db shop = db.Shop_db.Find(id);

            if (shop == null)
            {
                return HttpNotFound();
            }

            string currentImage = Request.MapPath(shop.ImagePath);
            db.Entry(shop).State = EntityState.Deleted;
            if (db.SaveChanges() > 0)
            {
                if (System.IO.File.Exists(currentImage))
                {
                    System.IO.File.Delete(currentImage);
                }
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}